$(document).ready(function() {
    $("#loginButton").click(function() {
        $("#loginModal").modal('show');
    });

    $("#signupButton").click(function() {
        $("#signupModal").modal('show');
    });

    $("#wellcomeModal").modal('show');

    //$('.button').popover({title: "What's up around here?", content: "Explore it and you'll find everything you need, if you are interested in space exploration. Also, you can contact us on space@what.who. Have a nice launch!"});

});